===============================================================================
                                     vtcol
===============================================================================

*******************************************************************************
                     color schemes for the Linux™ console
*******************************************************************************

:Date:                  2022-01-01
:Version:               0.42.7
:Manual section:        1
:Manual group:          console

Synopsis
-------------------------------------------------------------------------------

**vtcol** [--help] [--version] [--verbose] [--console <con>] <command> [<args>]

Description
-------------------------------------------------------------------------------

**vtcol** manipulates and inspects the color map of the Linux console
using ``ioctl_console(2)`` syscalls.

vtcol commands
-------------------------------------------------------------------------------

``vtcol`` actions are grouped into subcommands depending on the functionality
they operate on:

**help**

    Prints this message or the help of the given subcommand(s)

**colors**

    Inspect and manipulate the console palette.

**kb**

    Keyboard manipulation (LEds, modifiers).

vtcol colors commands
#####################

**set**

    Set the scheme for the current terminal with ``PIO_CMAP``. ::

        $ vtcol colors set solarized

**list**

    List predefined schemes. ::

        $ vtcol colors list
        4 color schemes available:
              * solarized
              * solarized_light
              * default
              * phosphor

    These are only the schemes built into the ``vtcol`` binary. Any
    other scheme can be loaded using the ``--file`` argument.

**dump**

    Print the palette of the specified scheme in more or less human readable
    representation.

**get**

    Get the current color scheme with ``GIO_CMAP``. The result is matched
    against the builtin schemes; if a scheme matches, only its preferred name
    is printed (e. g. ``solarized_light``). ::

        $ vtcol colors get
        solarized

    Otherwise the palette is printed as with the ``dump`` subcommand.

**toggle**

    Like ``vtcol colors set`` but supports two ``SCHEME`` arguments.

    First the active scheme is checked against *the first positional argument*;
    if there is a match, the scheme specified by the *second argument* will be
    loaded, otherwise the first one. The effect is that in repeated invocations
    of the same command the scheme is toggled between the two arguments. E. g.
    use: ::

        $ vtcol colors toggle solarized solarized_light

    to cycle the console palette between “dark mode” and “light mode”.

**fade**

    Transition between two color schemes with a fading effect. If no starting
    color scheme is specified, the current one is used. ::

    $ vtcol colors fade --ms 1337 --from solarized --to solarized_light

vtcol kb leds commands
######################

**get**

    Get the current LED state. Example: ::

        $ vtcol leds get
        caps: false, num: false, scroll: false

**set**

    Set the state of individual keyboard LEDs: ::

        $ vtcol leds set --caps on --num off

    Not that this command only affects the LEDs themselves. It will not change
    the state of the corresponding modifier locks. Revert the LEDs to normal:
    ::

        $ vtcol leds set --revert

vtcol kb flags commands
######################

**get**

    Get the current keyboard flags. Example: ::

        $ vtcol -C /dev/tty6 kb flags get
        [flags: caps: false, num: false, scroll: false; default: caps: false, num: false, scroll: false]

**set**

    Set the state of individual keyboard LEDs: ::

        $ vtcol flags set --caps on --num off


global options
-------------------------------------------------------------------------------

The options described in this section are not specific to a subcommand and can
be used everywhere on the command line.

``-v, --verbose``

    Enable some extra status messages.

``-C, --console CON``

    Operate on the console located at the path ``CON`` instead of the active
    one. Usually this is one of the ``/dev/tty*`` devices. For example, to
    get the scheme on a console some *getty*: ::

        $ vtcol colors get \
            --console $(readlink /proc/$(pgrep getty| head -1)/fd/0)

colors set options
-------------------------------------------------------------------------------
::

    vtcol colors set [--file FILE]

    vtcol colors set [SCHEME]

``SCHEME``

    Load the predefined color scheme *SCHEME*. For a list of options, use
    ``vtcol colors list``. Alternatively, ``-`` can be used to specify *stdin*.

``-f FILE, --file FILE``

    Load color scheme from *FILE* which can be a filesystem path or the special
    argument ``-`` to specify *stdin*.

    Mutually exclusive with *SCHEME*.

colors toggle options
-------------------------------------------------------------------------------

`vtcol colors toggle ONE TWO``

``ONE``

    First scheme to alternate between. Will be loaded *unless* it is the active
    scheme.

``TWO``

    Second scheme to alternate between. Will be loaded if *ONE* is the active
    scheme.

colors fade options
-------------------------------------------------------------------------------
::

    vtcol colors fade [--clear] [--frequency HZ] [--ms MS]
                      [--from START] --to END

``END``

    The scheme to transition to. After completion this will remain the active
    scheme of the current console.

``START``

    Load the predefined color scheme *SCHEME* before starting the transition.

``-f HZ, --frequency HZ``

    Screen update rate in ``HZ / s``.

``-m MS, --ms MS``

    Duration of the transition in milliseconds.

``-c, --clear``

    Whether to clear the console after each update step. This causes the
    background color to be applied to the entire screen and not just the parts
    that changed for a more uniform transition. The downside of ``--clear`` is
    that all text is erased as well.

Scheme file syntax overview
-------------------------------------------------------------------------------

Scheme files use a trivial line-based grammar: whatever hexadecimal RGB color
expression is found first on a line is used as the next color until the whole
palette of 16 colors is reached. ::

    $ cat schemes/zebra
    00#000000    white
    01#ffffff    black
    02#000000    white
    …
    15#ffffff   black

Everything before and after the color expression will be ignored. If the end
of the scheme file is reached without having assigned all colors, the remainder
will be set to zero (RGB 0, 0, 0).

Examples
-------------------------------------------------------------------------------

1. Get the current color scheme: ::

    $ vtcol colors get

2. Turn your screen into an 80s style monochrome lookalike: ::

    $ vtcol colors set phosphor

3. Reset console to default colors: ::

    $ vtcol colors set default

4. Set color scheme from stdin: ::

    $ <./schemes/zebra vtcol colors set -

5. List available builtin schemes: ::

    $ vtcol colors list

6. Cycle between night mode and day mode: ::

    $ vtcol colors toggle solarized solarized_light

7. Transition from dark to light scheme while dumping some data for effect: ::

    $ vtcol colors fade -m 1500 -f solarized -t solarized_light & dmesg

8. Dump current scheme in binary form: ::

    $ vtcol colors get --base64

9. Get the color scheme of a specific console: ::

    $ vtcol colors get --console /dev/tty6

Copyright
-------------------------------------------------------------------------------

Copyright: 2015-2022 Philipp Gesang

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License.

See also
-------------------------------------------------------------------------------

**ioctl_console**\(2), **tty**\(4)

The ``setcolors`` utility should be considered prior art, visit
`<https://github.com/EvanPurkhiser/linux-vt-setcolors>`.

Notes
-------------------------------------------------------------------------------

Upstream resides at `<https://gitlab.com/phgsgng/vtcol>`.

